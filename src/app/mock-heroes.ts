import { hero } from './hero';

export const HEROES: hero[] = [
  { id: 1, name: 'M90' },
  { id: 2, name: 'Narco' },
  { id: 3, name: 'Bombasto' },
  { id: 4, name: 'Felicidad' },
  { id: 5, name: 'Margarita' },
  { id: 6, name: 'Zien' },
  { id: 7, name: 'Dynama' },
  { id: 8, name: 'Dr House' },
  { id: 9, name: 'Magmacube' },
  { id: 10, name: 'Torreto' }
];